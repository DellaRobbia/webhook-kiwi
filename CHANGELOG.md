# CHANGELOG

Tous les changements notables de ce projet sont documentés ici.

Le format est basé sur [Keep a Changelog](http://keepachangelog.com/).

-   `Added` pour les nouvelles fonctionnalités.
-   `Changed` pour les changements aux fonctionnalités préexistantes.
-   `Deprecated` pour les fonctionnalités qui seront bientôt supprimées.
-   `Removed` pour les fonctionnalités désormais supprimées.
-   `Fixed` pour les corrections de bugs.
-   `Security` en cas de vulnérabilités.

## [0.4.2] - 2022-12-19

Module _ecowatt_.

### Added

-   ecowatt : commande pour obtenir les infos de l’API de RTE pour la surcharge à venir du réseau électrique français.

## [0.4.1] - 2021-08-01

### Changed

Création du salon de dev (`jardin de Kiwi`) pour éviter de spam le salon `spam-command`.

### Fixed

-   connexion rompue toutes les 8h dans le module beer : sql commit.
-   vérifications des fonctions pour la mise à jour du module discord sur le serveur de production

## [0.4.0] - 2021-01-01

### Added

-   k!vdm : création de la publication des vie de merdes des membres

## [0.3.2] - 2020-08-10

### Changed

-   k!beer : ajout des messages embed et interrogation de la base de données.
-   k!status : suppression des messages

### Fixed

-   k!beer : utilisateur totalement faux

## [0.3.1] - 2020-07-18

### Added

-   k!odd : génération d’une image d’Odd

### Changed

-   Prise en compte de la version du bot au lieu de la date

### Fixed

-   [log de gravatar](https://gitlab.com/DellaRobbia/webhook-kiwi/-/issues/1)

## [0.3.0] - 2020-07-18

### Changed

-   bot status : pour la mise à jour de discord.js v12

## [0.2.0] - 2020-07-??

### Added

-   Module Gravatar
-   Commande fox

## [0.1.0] - 2020-??-??

### Added

-   projet initial
-   ping, status, log, poll
