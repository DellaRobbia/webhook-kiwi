//extension pour Kiwi Bot
//module pour la commande gravatar
//https://github.com/emerleite/node-gravatar
// ©2020 Odd


//Constants
const gravatar = require('gravatar');


//fonctions
/*
var verif_email = (email) => {
  if (email.includes('@')) {
    return true;
  } else {
    return false;
  };
};
*/

//variables
var optionsGravatar = Array('404', 'mp', 'identicon', 'monsterid', 'wavatar', 'retro', 'robohash', 'blank');

//module
module.exports = {
  name: 'gravatar',
  description: 'Fonctions pour la génération d’un gravatar',
  execute(message, args) {
    //console.log(args);
    var email = args[0];
    if (args.length === 2) {
      var option = args[1];
    };
    //check email
    if (email.includes('@')) {
      var hash = gravatar.url(email);
    } else {
      message.channel.send('veuillez saisir une adresse mail valide');
      console.log('gravatar échec pour ' + email);
      return;
    };
    //Genération des gravatar
    if (args.length === 1) {
      message.channel.send('https:' + hash);
      console.log('gravatar réussi pour ' + email);
    } else {
      //prise en compte des paramètres
      if (args.length === 2 && optionsGravatar.includes(option)) {
        message.channel.send('https:' + hash + '?d=' + option + '&f=y');
        console.log('gravatar réussi pour ' + email + ' avec option ' + option);
      } else {
        // TODO: cas de plusieurs arguments genre taille et tout ?
        message.channel.send('Trop de paramètres ou un paramètre n’a pas été reconnu… donnez-moi un seul parmi la liste suivante : ' + optionsGravatar.join([separator = ', ']));
      };
    };
  },
};

/* Options disponibles
404: do not load any image if none is associated with the email hash, instead return an HTTP 404 (File Not Found) response
mp: (mystery-person) a simple, cartoon-style silhouetted outline of a person (does not vary by email hash)
identicon: a geometric pattern based on an email hash
monsterid: a generated 'monster' with different colors, faces, etc
wavatar: generated faces with differing features and backgrounds
retro: awesome generated, 8-bit arcade-style pixelated faces
robohash: a generated robot with different colors, faces, etc
blank: a transparent PNG image (border added to HTML below for demonstration purposes)
*/
console.log('module gravatar chargé');
