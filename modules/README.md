Dans ce répertoire il y aura tous les modules du bot qui sont des fonctions complexes.

Les modules suivants sont disponibles :
- `gravatar` sur la branche *gravatar* : `PREFIX gravatar <email> [option]`
- `ecowatt` sur la branche *ecowatt* : `PREFIX ecowatt [option]`

Les modules suivants sont en cours de développement :
- `beer` sur la branche *beer*
