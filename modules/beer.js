//extension pour Kiwi Bot
//module pour la commande bière
// ©2021 Odd

//Constants
const Discord = require('discord.js');
const mysql = require('mysql');

const enfantID = '700023335056179290';//rôle enfant


//imports configs
const config = require('../config.json');
const sqlconfig = config.mysql;
const emoji = config.emoji;

// imports fonctions
const sqlDB = require('../functions/fctSQL.js');

//fonctions
function getUserID(message, tableau, userTestName) {
  //tableau est var tableau = Array.from(message.guild.members.cache.keys());
  for (let indice in tableau) {
    try {
      var id = tableau[indice];
      let guildMember = message.guild.members.resolve(id);
      if (guildMember.user.username.toLowerCase() == userTestName.toLowerCase()) {
        return id;
      } else if (guildMember.nickname && guildMember.nickname.toLowerCase() == userTestName.toLowerCase()) {
        return id;
      } else {
        //message.channel.send(`Utilisateur·ice ${args[1]} non trouvé·e`);
      }
    } catch (e) {
      console.log(e);
      message.reply(userTestName + ' n’est pas un utilisateur connu…');
    }
  }
}

function embedMessage(color, title, author, body, thumbnail=null, fields=null, footer=null) {
  /*
  author : []
  fields : [{ name: 'Données', value: 'xx', inline: true }]
  */
  const embed = new Discord.MessageEmbed()
    .setColor(color)
    .setTitle(title)
    .setDescription(body);
    if (author) {
      if (author.length == 1) {
        embed.setAuthor(author[0])
      }else if (author.length == 2) {
        embed.setAuthor(author[0], author[1])
      }
    }
    if (thumbnail) {
      embed.setThumbnail(thumbnail);
    }
    if (fields) {
      fields.forEach((field, i) => {
        embed.addFields(field);
      });

    }
    if (footer) {
      embed.setFooter(footer);
    }
    return embed;
    /*
	.setThumbnail('https://i.imgur.com/wSTFkRM.png')
	.addFields(
		{ name: 'Regular field title', value: 'Some value here' },
		{ name: '\u200B', value: '\u200B' },//pour sauter une ligne
		{ name: 'Inline field title', value: 'Some value here', inline: true },
		{ name: 'Inline field title', value: 'Some value here', inline: true },
	)
	.addField('Inline field title', 'Some value here', true)//pour un autre champ
	.setImage('https://i.imgur.com/wSTFkRM.png')
	.setTimestamp()
	.setFooter('Some footer text here', 'https://i.imgur.com/wSTFkRM.png');
  */
} // commun embedMessage


//module
module.exports = {
  name: 'beer',
  description: 'Fonctions pour le module bière',
  help(message, idOdd){
    message.channel.send('Heu… ouais l’aide… <@'+idOdd+'> au secours… Nan mais de toute façon c’est en dev là…');
  },
  execute(client, message, args) {
    var author = message.member;
    //var user = message.author;
    var sqlQuery = '';

    switch (args[0]) {

      case 'shop':
        message.channel.send('Ouverture prochainement du *Bar Kiwi*');
        console.log('Magasin à faire');
      break;

      case 'test':
      sqlQuery = `select * from users where idDiscord = '${author.id}'`;
      var fonctionTest = async function(sqlQuery) {
        let result = await sqlDB.querySQL(sqlQuery);
        message.channel.send(`Vous avez ${result[0].solde} sous sur votre compte`);
      };
      fonctionTest(sqlQuery)
      break;

      case 'sous':
        //crédite le solde
        //let author = message.member;
        sqlQuery = `SELECT UNIX_TIMESTAMP(CURRENT_TIMESTAMP) - UNIX_TIMESTAMP(lastDaily) AS seconds FROM users WHERE idDiscord = '${author.id}'`;
        var fonctionSous = async function(sqlQuery) {
          let result = await sqlDB.querySQL(sqlQuery);
          let numberSeconds = result[0].seconds;
          let limiteSecond = 20 * 60 * 60 //20h
          if (numberSeconds >= limiteSecond) {
            sqlDB.querySQL(`UPDATE users SET solde = solde + 2 WHERE idDiscord = '${author.id}'`);
            message.channel.send(`${author.user.username}, vous avez gagné 2 sous !`);
            sqlDB.querySQL(`UPDATE users SET lastDaily = CURRENT_TIMESTAMP WHERE idDiscord = '${author.id}'`);
            console.log(`${author.user.username} + 2 sous`);
          } else {
            let tempsRestant = limiteSecond - numberSeconds
            message.reply(`Il faut attendre 20h entre les commandes sous. Pour vous il reste ${tempsRestant} secondes.`);
          }
        };
        fonctionSous(sqlQuery);
        //sqlDB.cloture;
        break;

      default:
        //Action de donner une bière à (args[0])
        // TODO: rajouter un message d'occasion : pourquoi offrir une bière…
        if (args.length === 1) {
          //Récupération de l'ID de l'user destinataire
          try {
            var targetID = getUserID(message,  Array.from(message.guild.members.cache.keys()), args[0]) || message.mentions.members.first().id;
          } catch (e) {
            message.reply(`${args[0]} n’est pas un utilisateur connu…`);
            return false;
          }
          if (targetID != author.id) {
            //l'user offre une boisson
            let guildMember = message.guild.members.resolve(targetID);
            let urlAvatar = 'https://cdn.discordapp.com/avatars/' + author.id + '/' + author.user.avatar + '.png';
            let authorArray = [author.user.username, urlAvatar];
            sqlDB.querySQL(`UPDATE placard SET données = données + 1  WHERE idDiscord = '${author.id}'`);
            sqlDB.querySQL(`UPDATE placard SET reçues = reçues + 1  WHERE idDiscord = '${targetID}'`);

            if (!guildMember._roles.includes(enfantID)) {
              //le membre n’est pas un enfant
              var title = 'offre une bière à'
              var description = `<@${targetID}>, *à la tienne !*`;
              var emojiThumbnail = emoji.beer;
              var footer = 'L’abus d’alcool est dangereux pour la santé, à consommer avec modération';
              console.log(`${author.user.username} donne une bière à ${message.guild.members.resolve(targetID).user.username}`);
            } else {
              // le membre est trop jeune pour boire de l'alcool
              var title = 'offre un chocolat chaud à'
              var description = `<@${targetID}>, *attention c’est chaud !*`;
              var emojiThumbnail = emoji.chocolat;
              var footer = 'Le chocolat, c’est la vie à consommer sans modération'
              message.reply(`Hé non malheureusement ${message.guild.members.resolve(targetID).user.username} est trop jeune pour boire de l'alcool…`);
              console.log(`${author.user.username} donne un chocolat à ${message.guild.members.resolve(targetID).user.username}`);
            }
            var fonctionEmbedBeer = async function(sqlQuery) {
              //récup info BD
              let result = await sqlDB.querySQL(sqlQuery);
              let données = result[0].données;
              let reçues = result[0].reçues;
              let fieldsArray = [{ name: 'Données', value: données, inline: true },
                { name: 'Reçues', value: reçues, inline: true }];
              //création du message embed
              textEmbed = embedMessage('#ff7a00', title, authorArray, description, thumbnail=emojiThumbnail, fields=fieldsArray, footer=footer);
              message.channel.send(textEmbed);
            };
            fonctionEmbedBeer(`SELECT * FROM placard WHERE idDiscord = '${author.id}'`);

          } else if (targetID == author.id) {
            //l'user boit
            var fonctionUserBoit = async function(sqlQuery) {
              //infos de la BD
              let result = await sqlDB.querySQL(sqlQuery);
              let buffer = await sqlDB.querySQL(`select * from users where idDiscord = '${author.id}'`);
              let possède = result[0].possède;
              let données = result[0].données;
              let reçues = result[0].reçues;
              let solde = buffer[0].solde;
              //infos discord
              let urlAvatar = 'https://cdn.discordapp.com/avatars/' + author.id + '/' + author.user.avatar + '.png';
              let authorArray = [author.user.username, urlAvatar];
              let fieldsArray = [{ name: 'Placard', value: possède, inline: true },
              { name: 'Soldes', value: solde, inline: true },{ name: 'Données', value: données, inline: true },
              { name: 'Reçues', value: reçues, inline: true }];
              let title = 'Santé'
              let description = `${author.user.username} boit une boisson`;
              textEmbed = embedMessage('#ff7a00', title, authorArray, description, thumbnail=null, fields=fieldsArray);
              message.channel.send(textEmbed);
            };
            fonctionUserBoit(`SELECT * FROM placard WHERE idDiscord = '${author.id}'`);
            //sqlDB.cloture;
            //message.channel.send(`${author.user.username} boit une boisson…`);
          } else {
            message.channel.send('Utilisateur inconnu');
          }
        } else {
          message.channel.send('Mauvais format de la commande');
        }
    }//fin du switch
  },
};

console.log("module bière chargé")
