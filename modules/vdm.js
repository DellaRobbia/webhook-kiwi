//extension pour Kiwi Bot
//module pour la commande vdm
// ©2020 Odd

//Constants
const Discord = require('discord.js');
const mysql = require('mysql');

const salonVdmId = '793896717950517288';//identification du salon VDM


//imports configs
const config = require('../config.json');
const sqlconfig = config.mysql;
const emoji = config.emoji;


//fonctions
const fct = require('../functions/communs.js');
const sqlDB = require('../functions/fctSQL.js');

function publication(texte, salon, user, id=0) {
  // publication d’un message embed
  if (user.username){
    var auteur = user.username;
  } else {
    var auteur = user;
  }
  textEmbed = fct.embedMessage('#9cff00', `#VDM ${id}`, [auteur], texte);
  if (config.DEV || config.DEBUG) {
    console.log((JSON.stringify(textEmbed, null, 2)));
  }
  salon.send(textEmbed);
}


//module
module.exports = {
  name: 'vdm',
  description: 'Fonctions pour le module VDM',
  help(message){
    message.channel.send('La commande VDM s’écrit : k!vdm |Ma vie c’est nul');
  },
  execute(client, message, args, idOdd){
    var user = message.author;
    if (config.DEV || config.DEBUG) {
      var salonVdm = client.channels.resolve(config.salon.debug);
      // TODO: faire un bd de dev
    } else {
      var salonVdm = client.channels.resolve(config.salon.salonVdmId);
    }

    if (args.length === 1) {
      // Récupérer l’id de la VDM
      sqlQuery = `SELECT idVDM FROM vdm ORDER BY idVdm desc LIMIT 1`; // SELECT MAX(idVDM) FROM vdm // TODO: créer une table de dev
      sqlQuery2 = `INSERT INTO vdm (idDiscord, texte) VALUES ('${user.id}', '${args}')`;
      (async () => {
        let result = await sqlDB.querySQL(sqlQuery);
        let newId = result[0].idVDM + 1
        // Publication Discord puis BbD
        console.log(args);
        publication(args[0], salonVdm, user, newId);
        sqlDB.querySQL(sqlQuery2);
        // TODO: vérifier que la requête est ok
        // informe l’auteur
        user.send("Votre VDM envoyée est `" + args + "` et son id est le " + newId);
        // log
        console.log(`new VDM #${newId} pour ${user.username}`);
      })(); // fin async

    } else if (args.length === 2) {
      // message avec un force auteur
      if (user.id === idOdd) {
        // Récupérer l’id de l’user
        // Récupérer l’id de la VDM
        // Publication
        publication(args[0], salonVdm, args[1])
        console.log(`new VDM pour ${args[1]}`);
      } else {
        message.channel.send('T’es pas Odd toi !');
        fct.grrrKiwi(message, user);
      }
    } else {
      message.channel.send('Trop d’arguments dans ta VDM…');
    }
  }
}

console.log("module vdm chargé")
