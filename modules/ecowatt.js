//extension pour Kiwi Bot
//module pour la commande ecowatt
//https://data.rte-france.com/catalog/-/api/consumption/Ecowatt/v4.0
// ©2022 Odd


//Constants
const config = require('../config.json');
let DEV = config.DEV;
let DEBUG = config.DEBUG;
//const Discord = require('discord.js');
const fs = require('fs');
const https = require('https');
const PATH_SIGNALS_JSON = './locale/var/ecowatt/signals.json';
const months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
const days = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"];
const URL_oauth = 'https://digital.iservices.rte-france.com/token/oauth/';
const URL_signals = 'https://digital.iservices.rte-france.com/open_api/ecowatt/v4/signals';



//variables

//fonctions
function getTokenOAUTH(token) {
  // return promise tokenOAUTH to use to download json file
  let options = {
    method: 'POST',
    headers: {'Authorization': `Basic ${token}`}
  };
  return new Promise(resolve => {
    https.get(URL_oauth, options, (res) => {
      if (DEBUG) {
        console.log('getTokenOAUTH:statusCode: ', res.statusCode);//console.log('headers:', res.headers);
      }
      res.on('data', (d) => {
        //process.stdout.write(d);
        let response = JSON.parse(d);
        let token_oauth = response.access_token;
        resolve(token_oauth);
      });
    })
    .on('error when got token OAUTH ', (e) => {
      console.error(e);
    });
  });
}

function getJSONFile(token_oauth) {
  // download signals.json from RTE server by API
  let options = {
    method: 'GET',
    headers: {'Authorization': `Bearer ${token_oauth}`}
  };
  return new Promise(resolve => {
    https
    .get(URL_signals, options, res => {
      if (DEBUG) {console.log('getJSONFile:statusCode: ', res.statusCode);}//console.log('headers:', res.headers);
      if (res.statusCode == 200) {
        const file = fs.createWriteStream(PATH_SIGNALS_JSON);
        // Write data into local file
        res.pipe(file);
        // Close the file
        file.on('finish', () => {
          file.close();
          if (DEBUG) {console.log(`New file signals.json downloaded !`);}
          resolve(res.statusCode);
        });
      } else {
        // may be error 429 : 1 call every 15 min
        resolve(res.statusCode);
      }
    })
    .on('error', err => {
      console.log('Error: ', err.message);
    });
  });
}

function downloadNewSignals(token) {
  // download signals json file by API
  return new Promise((resolve, reject) => {
    getTokenOAUTH(token).then(token_oauth => {
      getJSONFile(token_oauth).then(statusCode => {
        resolve(statusCode);
      });
    });
  });
}

function testSignalsExist(token) {
  // test la présence du fichier signals.json
  // return a bool for the test
  return new Promise((resolve, reject) => {
    fs.stat(PATH_SIGNALS_JSON, function(err, stat) {
      if (err == null) {
        // test de la date du fichier
        fs.readFile(PATH_SIGNALS_JSON, (err, data) => {
          if (err) throw err;
          let signals = JSON.parse(data).signals;
          let date_generation = new Date(signals[0].GenerationFichier);
          if (DEBUG) {console.log("testSignalsExist:GenerationFichier: " + date_generation);}
          //test si - 24h
          if ((Date.now() - date_generation) > 12 * 3600000) {
            // need to update
            resolve(false);
          } else {
            resolve(true);
          }
        });
      } else if (err.code === 'ENOENT') {
        // file does not exist need to download last signals
        resolve(false);
      } else {
        reject(err.code);
      }
    });
  });
}

function getDataInfo(argDay) {
  //parse data from signals json, by default data is for d day ; return string
  const regex = new RegExp('[jJ]([0-3])$');
  if (regex.test(argDay)) {
    var day = argDay.match(regex)[1];
  } else {
    var day = 'all';
  }
  if (DEBUG) {console.log(`getDataInfo ${argDay} -regex-> ${day}`);}
  return new Promise((resolve, reject) => {
    fs.readFile(PATH_SIGNALS_JSON, (err, data) => {
      if (err) throw err;
      // creation du texte message discord
      let signals = JSON.parse(data).signals;
      if (day == 'all') {
        let bloc_texte = "Prévision d’état du réseau électrique pour les 4 prochains jours\n";
        for (var j = 0; j < 4; j++) {
          let d = new Date(signals[j].jour);
          let array_hvalues = signals[j].values;
          let bare_24h = "0h";
          let buffer = {};
          buffer.date =  `${days[d.getDay()]} ${d.getDate()} ${months[d.getMonth()]} ${d.getFullYear()}`;
          switch (signals[j].dvalue) {
            case 1:
            buffer.dvalue = ':green_circle:';
            break;
            case 2:
            buffer.dvalue = ':orange_circle:';
            break;
            case 3:
            buffer.dvalue = ':red_circle:';
            break;
          }
          buffer.message = signals[j].message;
          array_hvalues.forEach((pas) => {
            switch (pas.hvalue) {
              case 1:
                bare_24h += ':green_square:';
                break;
              case 2:
                bare_24h += ':orange_square:';
                break;
              case 3:
                bare_24h += ':red_square:';
                break;
            }
          });
          buffer.pas = bare_24h + '24h';
          bloc_texte += `${buffer.date} : ${buffer.dvalue} (${buffer.message})\n${buffer.pas}\n\`Données en date du ${signals[j].GenerationFichier}\`\n`;
        } //end for
        resolve(bloc_texte);
      } else if (day >= 0 && day <= 3) {
        let d = new Date(signals[day].jour);
        let array_hvalues = signals[day].values;
        let bare_24h = "0h";
        let buffer = {};
        buffer.date =  `${days[d.getDay()]} ${d.getDate()} ${months[d.getMonth()]} ${d.getFullYear()}`;
        switch (signals[day].dvalue) {
          case 1:
            buffer.dvalue = ':green_circle:';
            break;
          case 2:
            buffer.dvalue = ':orange_circle:';
            break;
          case 3:
            buffer.dvalue = ':red_circle:';
            break;
        }
        buffer.message = signals[day].message;
        array_hvalues.forEach((pas) => {
          switch (pas.hvalue) {
            case 1:
              bare_24h += ':green_square:';
              break;
            case 2:
              bare_24h += ':orange_square:';
              break;
            case 3:
              bare_24h += ':red_square:';
              break;
          }
        });
        buffer.pas = bare_24h + '24h';
        resolve(`${buffer.date} : ${buffer.dvalue} (${buffer.message})\n${buffer.pas}\n\`Données en date du ${signals[day].GenerationFichier}\``);
      } else {
        console.error('impossible');
      }
    });
  });
}

//module
module.exports = {
  name: 'ecowatt',
  description: 'Fonctions pour la récupération des informations de l’API RTE EcoWatt',
  help(message){
    message.channel.send('La commande EcoWatt s’écrit : k!ecowatt [J0|J1|J2|J3|all]');
  },
  status(message, token) {
    //afficher les info du jour sur 24h
    //1 test signals file and created last 1h
    testSignalsExist(token)
      .then(parsing => {
        // parsing true or false
        if (!parsing) {
          if (DEBUG) {console.log("need to download signals.json");}
          downloadNewSignals(token)
            .then(statusCode => {
              if (statusCode == 200 | statusCode == 429) {
                getDataInfo('J0').then((texte) => message.channel.send(texte));
              } else {
                message.channel.send("Erreur dans l’acquisition du fichier de données : HTTP code " + statusCode);
              }
            })
            .catch(e => console.error(e));
        } else {
          //2 parse file to extract info
          if (DEBUG) {console.log("parsing signals.json");}
          getDataInfo('J0').then((texte) => message.channel.send(texte));
        }
      })
      .catch((e) => {
        console.log('Echec test if signals.json exist', e);
      });
  },
  predict(message, args, token) {
    //affiche les info des jour J (0 à 3) en fonction de la demande de l’user
    //1 test signals file and created last 1h
    testSignalsExist(token)
      .then(parsing => {
        //2 parse file to extract info
        return new Promise((resolve, reject) => {
          if (!parsing) {
            if (DEBUG) {console.log("need to download signals.json");}
            downloadNewSignals(token)
            .then(statusCode => {
              return new Promise((resolve, reject) => {
                if (statusCode == 200 | statusCode == 429) {
                  if (DEBUG) {console.log("file download with code " + statusCode);}
                  resolve(true);
                } else {
                  message.channel.send("Erreur dans l’acquisition du fichier de données : HTTP code " + statusCode);
                  resolve(false);
                }
              });
            }).then(b => resolve(b))
            .catch(e => console.error(e));
          } else {
            resolve(true);
          }
        });
      }).then(b => {
        //parsing file
        if (b) {
          if (DEBUG) {console.log("can parse signals.json");}
          getDataInfo(args).then((texte) => message.channel.send(texte));
        } else {
          message.channel.send("Erreur dans l’acquisition du fichier de données");
        }
      })
      .catch((e) => {
        console.log('Echec test if signals.json exist', e);
      });
  }
};

console.log('module ecowatt chargé');
