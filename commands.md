# Liste des commandes disponibles

Le préfix est `k!`, modifiable dans les paramètres.

## Commandes libres

Les listes des commandes que les users peuvent utiliser

### ping

Répond à l'user.
`@user, Wouaf ouaf`

### merde

Écrit un message.

### poll

Permet de réaliser un sondage.

Pour réaliser un sondage binaire, c’est-à-dire une question auquelle on poeut répondre par oui et non (ainsi qu’un « ne se pronce pas »), il fait utiliser le format suivant : `PREFIX poll |question`. Le bot supprime alors le message de l’utilisateur pour pouvoir proposer le sondlage sans générer de doublon.

Pour réaliser un sondage a proposition multiple (dans la limite actuelle de 9 propositions), le format de commande est le suivant : `PREFIX poll |Question|Choix 1|Choix 2|Choix n`. Le bot supprime alors le message original pour pouvoir proposé de façon propre le sondage. Notons que la commande originale est écrite en spoil pour permettre de réutiliser la commande si l’utilisateur c’est troomper dans l’ordre de ses choix.

La limite à 9 proposition dépend actuellement des 9 émotes de `A` à `I`.  

Un message d'erreur s’affiche si l'utilisateur ne rentre pas d'argument ou propose trop de choix.

### help

À FAIRE… Commande pour afficher l'aide, la liste des commandes.

### log

Commande pour afficher le `<message>` dans la console de débug (côté serveur)  
`PREFIX log <message>`

### gravatar

Commande permettant d'afficher le gravatar associé à une adresse email. La commande actuellement fonctionnelle est `PREFIX gravatar <email> [option]`.
Il est également possible de rajouter un paramètre pour l'affichage du gravatar en spécifiant cet option après l'email. Les options disponibles sont : '404', 'mp', 'identicon', 'monsterid', 'wavatar', 'retro', 'robohash' ou 'blank'.

### version

Commande pour afficher la version du bot actuellement en cours.  
`PREFIX version`

### boudaison

Cette commande permet à un utilisateur de s’attribuer le rôle `En Boudaison` et de se l’enlever quand bon lui semble. Les commandes sont respectivement : `PREFIX boudaison on` et `PREFIX boudaison off`.

### fox

Cette commande permet de récupérer une image de renard en utilisant l’API [https://randomfox.ca/](https://randomfox.ca/). Le format de la commande est : `PREFIX fox [numéro]`, avec numéro compris entre 1 et 122 inclus. Pour un tirage aléatoire il ne faut rien mettre.

### odd

Cette commande permet de récupérer une image d’Odd depuis le site de [codelyoko.fr](codelyoko.fr). Le format de la commande est : `PREFIX odd [numéro]`, avec numéro compris entre 1 et 1150 inclus. Pour un tirage aléatoire il ne faut rien mettre.

### beer

La commande est : `PREFIX beer [action] [user]`

### ecowatt

Cette commande permet d’obtenir les prévisions d’état de tensions du réseau électrique français. Les données sont transmises par RTE via une API.
La commande est : `PREFIX ecowatt [J0|J1|J2|J3|all]`.
Par défaut, l’info retournée est celle du jour (J0). Les prévisions sont disponible à J1 (le lendemain) à J3. Tout autre choix, l’option `all` est déclanché. L’option `all` renvoie toutes les infos de J0 à J3 inclus.

#### action par défaut

Cette commande permet d'offrir une bière (ou un chocolat chaud) à un utilisateur.
. S’il n'y a pas d'utilisateur, un message d'aide s’affiche et si l'utilisateur est l’auteur du message alors un message apparaît pour indiquer le nombre de bière que cet utilisateur a donnée et reçu.  
Si l'utilisateur possède le rôle `enfant` alors il recevra un chocolat chaud sinon ce sera une bière.

#### action : `sous`

Cette commande ne peut s'exécuter que toutes les 20h. Ceci permet de créditer le solde de l’utilisateur de 2 *sous*.

### vdm

La commande est : `PREFIX vdm |VDM_a_publier |auteur`

Cette commande permet de publier dans le salon `vdm` du serveur une histoire de vie de merde. Le paramètre auteur permet à l’administrateur de forcer l’auteur du message.<br>
Les VDM sont sauvegardées sur une base de données MySQL.

## Commandes spéciales

Commendes réservées à une certaine élite

### restart

Commande pour redémarrer le bot. Seul le maître de Kiwi peuvent lancer cette commande. Les intrus se feront gentillement aboyé dessus.

### debug

Pour passer le bot en mode débug ou le faire sortir.  
3 modes :
- normal `k!status online` : passe le bot en ligne, fonctionnement normal, statut *online*
- DEV `k!status dev` : passe le bot en mode developpement pour signaler aux utilisateurs les possibles bugs, statut *idle*
- DEBUG `k!status debug` : passe le bot en mode débuggage, seul l’admin peut lancer les commandes, statut *dnd*
