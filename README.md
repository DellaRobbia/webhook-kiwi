*en cours de rédaction*
*en cours de développement*

# Présentation

Bah voilà Kiwi c'est un bon chien. Du coup bah sur Discord c'est un bon bot Discord…

Sinon ce bot est simplement un projet perso pour apprendre comment coder une application *node.js* et également s’initier au *JavaScript*.

# Installation

Pour installer Kiwi, c'est très simple :

1. on clone le dépôt
2. on installe les dépendances :
  - discord : `npm install discord.js`
  - gravatar : `npm install gravatar`
  - mysql : `npm install mysql`
3. on change les paramètres (si besoin) pour activer ou désactiver les debug mode et configurer la clef token

# Mise à jour

Pour mettre à jour nodeJS :

0. `npm update -g`
1. MAJ de *npm* : `npm install -g npm`
2. MAJ de *NAME* : `npm update NAME`
3. audit : `npm audit fix`

Pour vérifier les versions des modules on peut passer par `npm ls`

# Configusation

Tout se passe dans les fichiers `config.json` et `configSecret.json`.

## `configSecret.json`

Ce fichier contient :
- l'ID Discord d’Odd qui est le maître de Kiwi (admin)
- le token du bot pour se connecter à Discord

## `config.json`

Ce fichier contient :
- la chaîne de caractère qui définie le préfixe des commandes
- les différents mode de développement (actif ou non)
- les différents emojis/png qui serviront à agrémenter les message embed de discord

# Les commandes disponibles

cf. `commands.md`
