Les modules *beer* et *vdm* ont besoin d’être connectés à des tables de données SQL.

# Données techniques

- MySQL
- `ENGINE = INNODB;`

# Description des tables

## `users`

Tables qui va comporter tous les utilisateurs du serveur. On part ici sur le fait qu'il n'y aura pas plus de 250 utilisateurs.
- idDiscord : 18 caractères → VARCHAR(20) PRIMARY KEY
- nom : VARCHAR(40)
- *avatar : pas utile avec l'idDiscord*
- *score : pas d'utilité pour l'instant*
- solde : monnaie virtuel INT UNSIGNED car toujours positif
- lastDaily : date de la dernière exécution de la commande → TIMESTAMP

## `boisson`
- idBoisson
- alcool
- catégorie
- nom
- degre
- prix

## `placard`
- idDiscord
- données
- reçues
- idBière

## `vdm`
- idDiscord
- idVdm : identification unique de la vdm de l’user
- texte : contenu de la VDM
- *score : pas d'utilité pour l'instant*

# Script SQL

## Création des tables

```SQL
--Table users
CREATE TABLE IF NOT EXISTS users (
  `idDiscord` VARCHAR(20) NOT NULL,
  `nom` VARCHAR(40),
  `solde` INT UNSIGNED DEFAULT 0,
  `lastDaily` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(`idDiscord`)
)
ENGINE=InnoDB;
--Table Boisson
CREATE TABLE IF NOT EXISTS boisson (
  `idBoisson` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(40) NULL DEFAULT NULL,
  alcool
  catégorie
  nom
  degre
  prix
  PRIMARY KEY (`idBoisson`)
)
ENGINE=InnoDB;
--Table Placard
CREATE TABLE IF NOT EXISTS placard (
  `idDiscord` VARCHAR(20) NOT NULL,
  `idBoisson` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `possède` INT UNSIGNED NOT NULL DEFAULT 0,
  `données` INT UNSIGNED NOT NULL DEFAULT 0,
  `reçues` INT UNSIGNED NOT NULL DEFAULT 0,
  CONSTRAINT fk_idDiscord
    FOREIGN KEY (`idDiscord`)
    REFERENCES users(`idDiscord`),
  CONSTRAINT fk_idBoisson
    FOREIGN KEY (`idBoisson`)
    REFERENCES boisson(`idBoisson`)
)
ENGINE=InnoDB,
COMMENT = 'Table contenant toutes les bières d’un user';
--Table vdm
CREATE TABLE IF NOT EXISTS vdm (
  `idDiscord` VARCHAR(20) NOT NULL,
  `idVdm` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `texte` TEXT NOT NULL,
  `score` INT UNSIGNED NOT NULL DEFAULT 0,
    FOREIGN KEY (`idDiscord`)
    REFERENCES users(`idDiscord`)
)
ENGINE=InnoDB,
COMMENT = 'Table contenant toutes les vdm d’un user';
```
