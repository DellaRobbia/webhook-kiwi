// This discord bot was created by Odd
// ©2022

//Constants
// Create an instance of a Discord client
const { Client, Intents } = require('discord.js');
const bot = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES] }); // https://discordjs.guide/popular-topics/intents.html


//imports configs
const config = require('./config.json');
const config2 = require('./configSecret.json');


//imports modules
const mod_gravatar = require('./modules/gravatar.js');
const mod_beer = require('./modules/beer.js');
const mod_vdm = require('./modules/vdm.js');
const mod_ecowatt = require('./modules/ecowatt.js');


//Variables
var DATE = '19-12-22';
var VERSION = require('./package.json').version;
var BRANCHE = 'master';
var emoteKiwi = '<:Kiwi:652643125277229059>';
var salonDebugLog = '862419200965214228';

let DEV = config.DEV;
let DEBUG = config.DEBUG;

if (DEV) {
	var description = 'KiwiBot ' + VERSION + "-dev_" + BRANCHE + '_' + DATE +  ' by ODD';
} else {
	var description = 'KiwiBot ' + VERSION + "_" + BRANCHE + ' by ODD';
}

const PREFIX = config.prefix;
const idOdd = config2.idAdmin;
const TOKEN = config2.token;

var alphabet = Array(':regional_indicator_a:',':regional_indicator_b:',
	':regional_indicator_c:',':regional_indicator_d:',':regional_indicator_e:',
  ':regional_indicator_f:',':regional_indicator_g:',':regional_indicator_h:',
  ':regional_indicator_i:');
var alphabetReact = Array('🇦', '🇧', '🇨', '🇩', '🇪', '🇫', '🇬', '🇭', '🇮');


//functions
var botStatus = (etat, description, type) => {
	bot.user.setPresence({
		activity: { name: description },
		type: {
      name: description,
      type: type //PLAYING: WATCHING: LISTENING: STREAMING:
    },
		status: etat });
}


//Statut du bot
bot.on('ready', function () {
	// TODO: revoir arrangement avec la boucle condition plus haut
  if (DEV) {
    var etatBot = 'idle';
  } else if (DEBUG) {
    var etatBot = 'dnd';
  } else {
    var etatBot = 'online';
  }
  botStatus(etatBot, description, 'PLAYING');
	if (DEV || DEBUG) {
		var salon = bot.channels.cache.get(salonDebugLog); // jardin
	} else {
		var salon = bot.channels.cache.get('647823515000766475'); // spam
	}
	//à changer en fonction du mode dev ou pas…
	if (DEV) {
		salon.send(emoteKiwi + ' ' + VERSION + '-dev de la branche ' + BRANCHE + ' et en date du ' + DATE + ' est chargée !');//channel spam
	} else {
		salon.send(emoteKiwi + ' ' + VERSION + ' de la branche ' + BRANCHE + ' est chargée !');
	}
	console.log(`Scanner ${bot.user.tag}`);
  console.log("Virtualisation "+ VERSION + ' Branche : ' + BRANCHE + `${DEV ? '-dev' : ''}` + `${DEBUG ? '-debug' : ''}` + ' en date du ' + DATE);
})


/////////////////////
//		COMMANDS  bot
/////////////////////
bot.on('message', msg => {
  //version
  if (msg.content === PREFIX + 'version') {
    msg.channel.send('Whouaf ! ' + emoteKiwi + ' ' + VERSION + ' de la branche ' + BRANCHE);
  }
  //ping
  if (msg.content === PREFIX + 'ping') {
    let pong = new Date().getTime() - msg.createdTimestamp;
    msg.reply('Wouaf ouaf\nPong de ' + pong + ' ms');
		console.log("ping/pong " + pong);
  }
  //transmission de message
  if (msg.content.startsWith(PREFIX + 'log')) {
    console.log(msg.content.slice(PREFIX.length).split(' ')); //c'est une liste
    msg.channel.send('Message transmis au serveur');
  }
  //gravatar
  if (msg.content.startsWith(PREFIX + 'gravatar')) {
		var query = msg.content.slice(PREFIX.length).split(' ');
		if (query.length === 1) {
			msg.reply("Ce n'est pas le bon format. La commande s’écrit : « k!gravatar email [paramètre] » (*paramètre étant optionnel et est : '404', 'mp', 'identicon', 'monsterid', 'wavatar', 'retro', 'robohash' ou 'blank'*)");
		} else {
			mod_gravatar.execute(msg, query.slice(1));
		}
	}
  //sondage
  if (msg.content.startsWith(PREFIX + 'poll')) {
    var arr = msg.content.slice(PREFIX.length).split('|');
		if (arr.length === 1) {
      msg.reply("Ce n'est pas le bon format. La commande s'écrit : « k!poll |Choix 1|Choix n »");
    }
    else if (arr.length === 2) {
			let post = `${msg.author.username} a posé la question suivante : « ${arr[1]} »`;
			msg.delete();
			msg.channel.send(post).then(sentMessage => {
				sentMessage.react('👍')
				sentMessage.react('👎')
				sentMessage.react('🤷');
			});
		}
		else if (arr.length <= 10){
			var post = "";
			let question = arr[1];
			msg.delete();
			msg.channel.send(`||${msg.content}||`);
			msg.channel.send(`${msg.author.username} demande : « ${question} »`)
			for (let i = 2 ; i < arr.length ; i++){
				post = post + alphabet[i-2] + " " + arr[i] + '\n'};
			msg.channel.send(post).then(sentMessage => {
			for (let i = 0 ; i < arr.length - 2 ; i++){
				sentMessage.react(alphabetReact[i])
      };
    });
  }
	else {
    let nbr = arr.length - 1;
		msg.channel.send('Trop de choix… ('+ nbr +'), <@'+idOdd+'>  ya du débug qui t’attend…')
	}}
	//boudaison
	// TODO: réinitialiser le salon boudaison
	if (msg.content.startsWith(PREFIX + 'boudaison')){
		let arr = msg.content.slice(PREFIX.length).split(' ');
		let nameRole = 'En Boudaison';
		let role = msg.guild.roles.cache.find(r => r.name === nameRole);
		//ajout du role
		if (arr[1] == 'on' && !msg.member.roles.cache.find(r => r.name === nameRole)) {
			msg.member.roles.add(role);
		}
		//retirer le role
		if (arr[1] == 'off' && msg.member.roles.cache.find(r => r.name === nameRole)) {
			msg.member.roles.remove(role);
		}
	}
	//get a fox picture
	if (msg.content.startsWith(PREFIX + 'fox')) {
		let arr = msg.content.slice(PREFIX.length).split(' ');
		//they are 122 pictures in database
		let number = Math.floor((Math.random() * 122) + 1);
		let url = 'https://randomfox.ca/images/'
		if (arr.length == 1) {
			msg.channel.send('https://randomfox.ca/images/' + number + '.jpg');
		}
		else if (arr[1] <= 122 && arr[1] >= 1) {
			msg.channel.send('https://randomfox.ca/images/' + arr[1] + '.jpg');
		} else {
			msg.reply('Il faut soit choisir un nombre entre 1 et 122 inclus ou ne rien mettre')
		}
	}
	//photo de mon Dieu
	if (msg.content.startsWith(PREFIX + 'odd')) {
		let arr = msg.content.slice(PREFIX.length).split(' ');
		//convertir le number en string format [0-9]*4
		let url = 'http://img.codelyoko.fr/galeries/odd2/Odd_'
		if (arr.length == 1) {
			//they are 1150 pictures in database cl.fr
			var number = Math.floor((Math.random() * 1150) + 1).toString();
		}
		else if (arr[1] <= 1150 && arr[1] >= 1) {
			number = arr[1];
		} else {
			msg.reply('Il y a 1150 images de notre Dieu, il faut donc choisir un nombre entre 1 et 1150 inclus ou ne rien mettre');
			return;
		}
		while (number.length <= 3) {
			number = '0' + number;
		}
		console.log(url + number + '.jpg');
		msg.channel.send(url + number + '.jpg');
	}
	//bière
	if (msg.content.startsWith(PREFIX + 'beer')) {
		let arr = msg.content.split(' ');
		if (arr.length === 1) {
			mod_beer.help(msg, idOdd);
		} else {
			mod_beer.execute(bot, msg, arr.slice(1));
		}
	}
	//VDM
	if (msg.content.startsWith(PREFIX + 'vdm')) {
		let arr = msg.content.split('|'); // on transmets ce qui ya après la commande
		if (arr.length === 1) {
			mod_vdm.help(msg)
		} else {
			mod_vdm.execute(bot, msg, arr.slice(1), idOdd)
		}
	}
	//EcoWatt
	if (msg.content.startsWith(PREFIX + 'ecowatt')) {
		// need token API RTE from config_secret
		let arr = msg.content.split(' ');
		if (arr.length === 1) {
			mod_ecowatt.status(msg, config2.token_API_RTE)
		} else if (arr.length === 2) {
			mod_ecowatt.predict(msg, arr[1], config2.token_API_RTE)
		} else {
			mod_ecowatt.help(msg)
		}
	}
  //played
  // TODO: faire une fonction pour remplacer celle de Tatsu
  //épisode de Code Lyokô
  // TODO: choisir un épisode au hasard
  //help
  // TODO: faire le k!kelp
//end commands bot
});


/////////////////////
//		MANAGE bot
/////////////////////
// restart bot
bot.on('message', msg => {
  //fonction réponse des intrus
  var replyIntrus = (user, id) => {
    msg.reply('je n’obéis qu’à mon maître Odd !')
    msg.channel.send('https://tenor.com/view/code-lyoko-kiwi-dog-animated-grunt-gif-17137093')
		console.log(`Un intru (${user} ${id}) a essayé de toucher à Kiwi`);
  }
  //restart
	if (msg.content === PREFIX + 'restart' && msg.author.id === idOdd) {
		console.log('restart…');
		msg.channel.send('Procédure de redémarrage du bot en cours…')
		.then(msg => setTimeout(process.exit(), 250));//pour que PM2 relance le bot, 500 pour laisser le temps au message de partir ? maybe…
		console.log('…done');
	}
	else if (msg.content === PREFIX + 'restart' && msg.author.id != idOdd) {
		replyIntrus(msg.author.username, msg.author.id);
	}
  //status mode
  if (msg.content.startsWith(PREFIX + 'status') && msg.author.id === idOdd) {
		let args = msg.content.slice(PREFIX.length).split(' ');
		if (args[1] == 'dev') {
			etatBot = "idle";
			DEBUG = false;
			DEV = true;
			msg.channel.send('Je passe en mode DEV… (*planquez-vous… ça peut péter…*)');
			console.log('DEV MODE ON');
		}
		else if (args[1] == 'debug') {
			etatBot = "dnd";
			DEV = false;
			DEBUG = true;
			msg.channel.send('Je suis fatigué, je vais dans ma niche… (*je ne vous réponds plus*)');
			console.log('DEBUG MODE ON');
		}
		else if (args[1] == "online") {
			etatBot = "online";
			DEV = false;
			DEBUG = false;
			msg.channel.send('Je passe en mode en mode normal… (*ouf… ça s’arrête enfin*) Du coup je retourne dans ma niche.');
			console.log('ONLINE MODE ON');
		}
		else {
			msg.channel.send(`La version en cours de Kiwi est : ` + emoteKiwi + ' ' + VERSION + ' de la branche ' + BRANCHE + ' et en date du ' + DATE);
			console.log("mode dev "+ DEV + ", debug " + DEBUG);
			return 0;
		}
		botStatus(etatBot, description, "PLAYING");
	}
	else if (msg.content.startsWith(PREFIX+'status') && msg.author.id != idOdd) {
		replyIntrus(msg.author.username, msg.author.id);
  }
})


//:D
bot.on('message', message => {
  if (message.content === 'merde') {
    message.reply('reste poli… c’est <@456922997203140612> qui ne va pas être content…');
    //const user = bot.users.cache.get(message.author.send());
		//user.send('Coucou');
  }
})


//test
bot.on('message', message => {
	var salon = bot.channels.cache.get(salonDebugLog);

  if (message.content.startsWith('k!test') && DEV) {
    const {MessageEmbed} = require('discord.js')
      salon.send("print('NPM nik bien ta race')", {code: 'python'});
      salon.send('This is an embed', {
          embed: {
              title: 'Renard',
              description: 'Renard image',
              image: {url: 'https://randomfox.ca/images/22.jpg'}
          }}
      )
  	.catch(console.error);
		console.log("Commande test");
	}
})

//Connection du bot
bot.login(TOKEN);
