//fonctions pour Kiwi Bot
//ensemble de fonctions communes pour KiwiBot
// ©2021 Odd

//Constants
const Discord = require('discord.js');


function getUserID(message, tableau, userTestName) {
  //tableau est var tableau = Array.from(message.guild.members.cache.keys());
  for (let indice in tableau) {
    try {
      var id = tableau[indice];
      let guildMember = message.guild.members.resolve(id);
      if (guildMember.user.username.toLowerCase() == userTestName.toLowerCase()) {
        return id;
      } else if (guildMember.nickname && guildMember.nickname.toLowerCase() == userTestName.toLowerCase()) {
        return id;
      } else {
        //message.channel.send(`Utilisateur·ice ${args[1]} non trouvé·e`);
      }
    } catch (e) {
      console.log(e);
      message.reply(userTestName + ' n’est pas un utilisateur connu…');
    }
  }
}


module.exports = {
  // message d’alerte quand l’user n’est pas Odd
  grrrKiwi(msg, user) {
    msg.reply('je n’obéis qu’à mon maître Odd !');
    msg.channel.send('https://tenor.com/view/code-lyoko-kiwi-dog-animated-grunt-gif-17137093');
    console.log(`Un intru (${user.username} ${user.id}) a essayé de toucher à Kiwi`);
  },

  // création de message embed
  embedMessage(color, title, author, body, thumbnail=null, fields=null, footer=null) {
    // retourne l’objet prêt à être posté
    const embed = new Discord.MessageEmbed()
      .setColor(color)
      .setTitle(title)
      .setDescription(body);
      //.setTimestamp();
      if (author) {
        if (author.length == 1) {
          embed.setAuthor(author[0]);
        }else if (author.length == 2) {
          // pour les liens urls de l’auteur : doit être http ou https
          embed.setAuthor(author[0], author[1]);
        }
      }
      if (footer) {
        if (footer.length == 1) {
          embed.setFooter(footer[0]);
        }else if (footer.length == 2) {
          // pour les liens urls icon du footer doit être http ou https
          embed.setFooter(footer[0], footer[1]);
        }
      }
      return embed;
      //.setAuthor(author)
      /*if (thumbnail) {
        embed.setThumbnail(thumbnail);
      }
      if (fields) {
        for (var index in fields) {
          //console.log(fields[index]);
          embed.addFields(fields[index]);
        }
      }*/
      /*
  	.setURL('https://discord.js.org/')
  	.setAuthor('Some name', 'https://i.imgur.com/wSTFkRM.png', 'https://discord.js.org')
  	.setDescription('Some description here')
  	.setThumbnail('https://i.imgur.com/wSTFkRM.png')
  	.addFields(
  		{ name: 'Regular field title', value: 'Some value here' },
  		{ name: '\u200B', value: '\u200B' },//pour sauter une ligne
  		{ name: 'Inline field title', value: 'Some value here', inline: true },
  		{ name: 'Inline field title', value: 'Some value here', inline: true },
  	)
  	.addField('Inline field title', 'Some value here', true)//pour un autre champ
  	.setImage('https://i.imgur.com/wSTFkRM.png')
  	.setFooter('Some footer text here', 'https://i.imgur.com/wSTFkRM.png');
    */
  }
};
