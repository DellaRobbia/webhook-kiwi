//fonctions pour Kiwi Bot
//ensemble de fonctions sql pour KiwiBot
// ©2021 Odd

//Constants
const Discord = require('discord.js');
const mysql = require('mysql');
const config = require('../config.json');

const sqlconfig = config.mysql;

module.exports = {
  //fonction pour faire des reqêtes SQL

  async querySQL(requete) {
    return new Promise(function(resolve, reject) {
      var returnValue = "";

      // paramètres pour la BdD
      const sql = mysql.createConnection({
        host: sqlconfig.host,
        user: sqlconfig.username,
        password: sqlconfig.password,
        database: sqlconfig.database
      });

      sql.connect(function(err) {
        if (err) throw err;
        if (config.DEV || config.DEBUG) {
          console.log("Connexion à la BdD…");
        }
      })

      sql.query(requete, function(error, rows) {
        if (error) {
          returnValue = "";
        } else {
          returnValue = rows;
          if (config.DEV || config.DEBUG) {
            console.log(requete);
            console.log(rows);
          }
        }
        resolve(returnValue);
        //sql.end();
      });
    });
  },

  cloture() {
    // utilité ??
    sql.end(function(err) {
      if (err) throw err;
      if (config.DEV || config.DEBUG) {
        console.log("Connexion à la BdD terminée.");
      }
    })
  }
}
